export function buildQueryString(params) {
  return Object.keys(params).map(key => `${key}=${params[key]}`).join("&");
}

export function buildRequestUrl(opts) {
  return `${opts.endpoint}?${buildQueryString(opts.params)}`;
}

export function buildRequestPath(opts) {
  return `${opts.endpoint}?${buildQueryString(opts.params)}`;
}

export const freqFlyerService = "/insuranceService/";
export const freqFlyerCRM = "/insuranceCRM/";
export const getInsuranceData = "insuranceCRM/insuranceDetails/filter";
export const downloadConnectorReport = "insuranceConnector/reports";
export const download = "insurance/reports"
export const status = [
  {
    value: "PENDING",
    title: "PENDING",
    selected: true
  },
  {
    value: "INPROGRESS",
    title: "INPROGRESS"
  },
  {
    value: "UPDATED",
    title: "UPDATED"
  }
];

export const headers = [
  {
    key: "S.NO",
    title: "S.NO",
    sortDisabled: true,
  },
  {
    key: "tripId",
    title: "TRIP ID",
    sortDisabled: true,
  },
  {
    key: "createdAt",
    title: "CREATED AT",
  },
  {
    key: "updatedAt",
    title: "UPDATED AT",
    sortDisabled: true,
  },
  {
    key: "status",
    title: "STATUS",
    sortDisabled: true,
  }
];

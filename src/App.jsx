import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "./App.css";
import Header from "./components/libs/Header";
import InsuranceInterface from "./containers/Insurance";
import InsuranceReport from "./containers/InsuranceReport";

class App extends Component {
  handle = () => {
  }
  render() {
    return (
      <div>
        <Router basename="/fcrm" >
          <div>
            <Header />
            <Route exact={true} path="/" component={InsuranceInterface} />
            <Route path="/insurancereport" component={InsuranceReport} />
          </div>
        </Router>
      </div>
    );
  }
}

export default App;

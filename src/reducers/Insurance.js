import { FETCH_DATA, STATUS_UPDATE, PAGE_IS_LOADING, APPLICATION_HAS_ERRORED, FAILURE, DIALOG_CLOSE } from "../actiontypes/InsuranceActionType";

const initialState = {
  list: [],
  responseStatus: 0,
  isLoading: true,
  dialogOpen: true,
  searchRequest: {
    filters: { status: "PENDING" },
    search: { emailSearch: "8997" },
    to: { toSearch: "87345678" },
    from: { fromSearch: "876" },
    trip: { tripSearch: "100800" },
    pagination: {
      PageNo: 1,
      lastPage: false,
      lastPageNumber: 1,
    }
  }
};

const Insurance = (state = initialState, action) => {
  switch (action.type) {
  case FETCH_DATA: {
    const { list, currentSearchRequest: searchRequest } = action.data;
    return {
      ...state,
      list,
      searchRequest,
      isLoading: false,
    };
  }
  case STATUS_UPDATE: {
    const { list, searchRequest } = state;
    const {
      rowIndex, freqFlyerStatus
    } = action.data;
    const currentRow = { ...list[rowIndex], freqFlyerStatus };
    list[rowIndex] = currentRow;
    return {
      ...state,
      list: [...list],
      ...searchRequest,
      // list
    };
  }
  case DIALOG_CLOSE: {
    const { dialogStatus } = action.data;
    return {
      ...state,
      dialogOpen: dialogStatus
    };
  }

  case APPLICATION_HAS_ERRORED: {
    const { hasErrored, errorMessage } = action;
    return {
      ...state,
      hasErrored,
      errorMessage
    };
  }

  case FAILURE: {
    const { hasFailure, apiMessage } = action;
    return {
      ...state,
      hasFailure,
      apiMessage
    };
  }
  case PAGE_IS_LOADING: {
    const { isLoading, loadMessage } = action;
    return {
      ...state,
      isLoading,
      loadMessage
    };
  }
  default:
    return state;
  }
};

export default Insurance;

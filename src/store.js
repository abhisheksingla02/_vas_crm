import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { createLogger } from "redux-logger";
import insurance from "./reducers/Insurance";

const crud = combineReducers({
  insurance
});

const logger = createLogger({
  // ...options
});
const middlewares = [
  crud,
  applyMiddleware(thunk)
];
if (process.env.NODE_ENV === "development") {
  middlewares.push(applyMiddleware(logger));
}
export default createStore.apply(null, [...middlewares]);

import React, { Component } from "react";
import PropTypes from "prop-types";
import styles from "../../css/FrequentFlyerCss";
import Row from "../libs/Row";
import Cell from "../libs/Cell";
import DropDown from "../libs/Dropdown";
import Buttons from "../libs/Buttons";
import { status as statuses } from "../../constants/Insurance";

const styl = ({
  cellAlignLeft: {
    textAlign: "left"
  },
  cellAlignRight: {
    textAlign: "right"
  },
  anchor: {
    textDecoration: "none",

  },
});
class FrequentFlyerRow extends Component {
  static propTypes = {
    flyer: PropTypes.objectOf(PropTypes.string),
    rowIndex: PropTypes.number,
    searchRequest: PropTypes.objectOf(PropTypes.any).isRequired,
    onStatusUpdate: PropTypes.func.isRequired,
    freqFlyerPhoneNo: PropTypes.number,
    onDialogClose: PropTypes.func.isRequired
  }
  static defaultProps = {
    flyer: {},
    rowIndex: 0,
    freqFlyerPhoneNo: 0,
  }
  state = {
    freqFlyerStatus: this.props.flyer.freqFlyerStatus,
    rowIndex: this.props.rowIndex,
    freqFlyerPhoneNo: this.props.flyer.freqFlyerPhoneNo,
    dialogStatus: this.props.searchRequest.dialogStatus,
    verificationId: this.props.flyer.verificationId
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      freqFlyerStatus: nextProps.flyer.freqFlyerStatus,
      rowIndex: nextProps.rowIndex,
      freqFlyerPhoneNo: nextProps.flyer.freqFlyerPhoneNo,
      dialogStatus: nextProps.searchRequest.dialogStatus
    });
  }
  _handleUpdateStatus = () => {
    this.setState({ dialogStatus: true });
    return this.props.onStatusUpdate({
      freqFlyerStatus: this.state.freqFlyerStatus,
      rowIndex: this.state.rowIndex,
      freqFlyerPhoneNo: this.state.freqFlyerPhoneNo,
      dialogStatus: this.state.dialogStatus,
      verificationId: this.state.verificationId
    });
  }
  _handleDialogBox = (dialogStatus) => {
    this.setState({ dialogStatus });
    return this.props.onDialogClose({
      dialogStatus
    });
  };
  _handleRowStatusChange = (freqFlyerStatus) => {
    this.setState({
      freqFlyerStatus,
    });
  }
  render() {
    const { flyer, rowIndex } = this.props;
    const { freqFlyerStatus } = this.state;
    return (
      <Row
        key={flyer.id}
        style={ (rowIndex) % 2 === 0 ? {
          backgroundColor: "white",
          paddingTop: "15px",
          paddingBottom: "15px"
        } : {
          backgroundColor: "#E0E0E0",
          paddingTop: "15px",
          paddingBottom: "15px"
        } }
      >
        <Cell>{rowIndex + 1}</Cell>
        <Cell style={styl.cellAlignLeft}>{flyer.email}</Cell>
        <Cell style={styl.cellAlignLeft}>{flyer.phone}</Cell>
        <Cell style={styl.cellAlignLeft}>{flyer.submissionDate}</Cell>
        <Cell>
          {flyer.loyaltyMembershipImageUrl !== "No Data Found" && flyer.loyaltyMembershipImageUrl !== "NA" && flyer.loyaltyMembershipImageUrl !== null ? <a target="_blank" href={flyer.loyaltyMembershipImageUrl} style={styl.anchor}>Documents Link</a> : "No Data Found" }
        </Cell>
        <Cell><DropDown
          items={statuses}
          style={styles.textPropertiesDropDown}
          index={this.state.rowIndex}
          id={this.state.id}
          selectedvalue={freqFlyerStatus}
          onChange={this._handleRowStatusChange}
          disabled={flyer.freqFlyerStatus !== "PENDING" ? true : null}
        />
        </Cell>
        <Cell>
          <Buttons style={{ fontWeight: "bold" }} disabled={flyer.freqFlyerStatus !== "PENDING"} variant="raised" color="primary" label="update" onClick={this._handleUpdateStatus} />
        </Cell>
      </Row>
    );
  }
}

export default FrequentFlyerRow;

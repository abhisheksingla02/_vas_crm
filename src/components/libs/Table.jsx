import React, { Component } from "react";
import PropTypes from "prop-types";
import Table, { TableCell, TableHead, TableBody } from "material-ui/Table";
// import Row from "./Row";
import { withStyles } from "material-ui/styles";
import Paper from "material-ui/Paper";
// import TextFields from "./TextField";
// import styl from "./../css/common";

const styles = ({
  root: {
    width: "100%",
    // marginTop: theme.spacing.unit * 1,
    overflowX: "auto",
    textAlign: "center",
  },
  table: {
    tableLayout: "relative",
    // overflow: "hidden"
  },
  tableHead: {
    align: "center",
    cellPadding: 2,
    cellSpacing: 2,
    textAlign: "center",
    fontFamily: "-apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Ubuntu,Arial,sans-serif",
    fontWeight: "Bold",
    fontSize: "100%",

  },
  headings: {
    fontWeight: "900",
    fontSize: "16px",
    textAlign: "center",
    // width: "100%"
  }
});
class FFTable extends Component {
_handle = () => {

}
render() {
  const {
    headers,
    classes
  } = this.props;
  return (
    <div>
      <Paper
        className={classes.root}
        style={{
          boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)",
          backgroundColor: "rgb(162, 162, 162)"
        }}
      >
        <Table className={classes.table}>
          <TableHead className={classes.tableHead}>
            {headers.map(head => (
              <TableCell
                key={head.key}
                className={ classes.headings }
                style={{
                  width: head.width,
                  color: "white",
                  paddingTop: "7px",
                  paddingBottom: "7px"
                }}
              >
                {head.title}
              </TableCell>))}
          </TableHead>
          <TableBody >
            {this.props.children}
          </TableBody>
        </Table>
      </Paper>
    </div>
  );
}
}

FFTable.propTypes = {
  children: PropTypes.arrayOf(PropTypes.object),
  headers: PropTypes.arrayOf(PropTypes.object),
  // handleChangeFilters: PropTypes.func,
  classes: PropTypes.objectOf(PropTypes.object).isRequired,

};
FFTable.defaultProps = {
  headers: [],
  children: [],
  // handleChangeFilters: () => { },
};
// export default FFTable;

export default withStyles(styles)(FFTable);

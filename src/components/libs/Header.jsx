import { Link, withRouter } from "react-router-dom";
import React, { Component } from "react";
import PropTypes from "prop-types";
import AppBar from "material-ui/AppBar";
import Toolbar from "material-ui/Toolbar";
import Buttons from "./Buttons";
import Drawer from "./MobileHeaderDrawer";
import logo from "../../assets/images/logo.png";

class Header extends Component {
  handle = () => {
  }
  render() {
    const { location: { pathname } } = this.props;
    return (
      <div>
        <AppBar position="static" style={{ backgroundColor: "#f1f1f1" }} >
          <Toolbar>
            <span style={
              window.innerWidth >= "600" ?
                {
                  display: "none"
                } : {
                  display: "block"
                }
            }
            >
              <Drawer />
            </span>
            <span >
              <img style={{ float: "right" }} src={logo} height="30" width="140" alt="logo" />
            </span>
            {"\u00A0"}
            {"\u00A0"}
            <span style={
              window.innerWidth <= "600" ?
                {
                  display: "none"
                } : {
                  display: "block"
                }
            }
            >
              <span>
                <Link to="/" style={{ textDecoration: "none" }}>
                  <Buttons
                    style={{ fontWeight: "bold", fontFamily: "-apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Ubuntu,Arial,sans-serif", position: "relative" }}
                    label="Insurance"
                    color={pathname === "/" ? "secondary" : "primary"}
                    variant="flat"
                  />
                </Link>
              </span>
              <span>
                <Link to="/insurancereport" style={{ textDecoration: "none" }}>
                  <Buttons
                    style={{
                      fontWeight: "bold",
                      fontFamily: "-apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Ubuntu,Arial,sans-serif",
                      position: "relative"
                    }}
                    label="Insurance Report"
                    color={pathname === "/insurancereport" ? "secondary" : "primary"}
                    variant="flat"
                  />
                </Link>
              </span>
            </span>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

Header.propTypes = {
  location: PropTypes.objectOf(PropTypes.string).isRequired,
};

export default withRouter(Header);

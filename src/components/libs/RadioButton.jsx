import React, { Component } from "react";
import { withStyles } from "material-ui/styles";
import Radio, { RadioGroup } from "material-ui/Radio";
import { FormLabel, FormControl, FormControlLabel } from "material-ui/Form";

const styles = theme => ({
  root: {
    display: "flex",
  },
  formControl: {
    margin: theme.spacing.unit * 1,
  },
  group: {
    margin: `${theme.spacing.unit}px 0`,
  },
});

class RadioButtonsGroup extends Component {
  state = {
    value: "Pending",
  };

  _handleChange = (event) => {
    this.setState({ value: event.target.value });
    /* console.log(this.state.value, event.target.value); */
  };

  render() {
    return (
      <div>
        <FormControl component="fieldset" required={ true }>
          <FormLabel component="legend">Status</FormLabel>
          <RadioGroup
            value={ this.state.value }
            onChange={ this._handleChange }
          >
            <FormControlLabel value="Approved" control={ <Radio /> } label="Approved" />
            <FormControlLabel value="Pending" control={ <Radio /> } label="Pending" />
            <FormControlLabel value="Rejected" control={ <Radio /> } label="Rejected" />
          </RadioGroup>
        </FormControl>
      </div>
    );
  }
}

export default withStyles(styles)(RadioButtonsGroup);

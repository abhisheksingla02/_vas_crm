import React, { Component } from "react";
import PropTypes from "prop-types";
import { TableRow } from "material-ui/Table";

class Row extends Component {
  handle = () => {

  }
  render() {
    return (
      <TableRow {...this.props } style={ this.props.style }>
        {this.props.children}
      </TableRow>
    );
  }
}

Row.propTypes = {
  children: PropTypes.any, //eslint-disable-line
  style: PropTypes.objectOf(PropTypes.any)
};
Row.defaultProps = {
  style: {}
};
export default Row;

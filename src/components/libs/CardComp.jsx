import React, { Component } from "react";
import Card, { CardContent, CardHeader } from "material-ui/Card";
import propTypes from "prop-types";

const styles = {
  root: {
    maxHeight: 800
  }
};
class CardComp extends Component {
 handle = () => {

 }

 render() {
   return (
     <div>
       <Card {...this.props } style={ styles.root } variant="raised" >
         <CardHeader title={ this.props.title } />
         <CardContent>
           { this.props.label }
         </CardContent>
       </Card>
     </div>
   );
 }
}

CardComp.propTypes = {
  title: propTypes.string,
  label: propTypes.node.isRequired
};

CardComp.defaultProps = {
  title: "",
};

export default CardComp;

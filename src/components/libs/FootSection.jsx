import React from "react";
import PropTypes from "prop-types";
import { List, ListItem, withStyles } from "material-ui";
import { footerStyle } from "../../css/footerStyle";

const d = new Date();
function Footer({ ...props }) {
  const { classes } = props;
  const links = [
    { title: "About Us", href: "https://www.cleartrip.com/about" },
    { title: "Privacy Policy", href: "https://www.cleartrip.com/privacy-policy/" },
    { title: "Security", href: "https://www.cleartrip.com/security/" },
    { title: "Term of use", href: "https://www.cleartrip.com/terms/" },
    { title: "Contacts", href: "https://www.cleartrip.com/contact/" },
  ];
  return (
    <footer className={classes.footer}>
      <hr />
      <div>
        <div className={classes.left}>
          <List>
            {links.map((link) => {
              return [
                <ListItem className={classes.inlineBlock}>
                  <a
                    href={ link.href }
                    style={{
                      color: "#3366cc",
                      textDecoration: "none",
                      fontSize: "14px"
                    }}
                  >{ link.title }
                  </a>
                </ListItem>
              ];
            })}
            <ListItem style={{ fontSize: "13.333px", color: "rgb(153, 153, 153)" }}>
            2006-{d.getFullYear()} Cleartrip Private Limited
            </ListItem>
          </List>
        </div>
      </div>
    </footer>
  );
}

Footer.propTypes = {
  classes: PropTypes.objectOf.isRequired,
  style: PropTypes.objectOf(PropTypes.any)
};

Footer.defaultProps = {
  style: {}
};

export default withStyles(footerStyle)(Footer);

import React, { Component } from "react";
import Button from "material-ui/Button";
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,

} from "material-ui/Dialog";

class Confirm extends Component {
  state = {
    open: false,
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    return (
      <div>
        <Button variant="raised" color="primary" onClick={ this.handleClickOpen }>Save</Button>
        <Dialog
          open={ this.state.open }
          onClose={ this.handleClose }
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Are you sure you want to save changes?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={ this.handleClose } color="primary">
              Review
            </Button>
            <Button onClick={ this.handleClose } color="primary" autoFocus={ true }>
              Yes
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default Confirm;

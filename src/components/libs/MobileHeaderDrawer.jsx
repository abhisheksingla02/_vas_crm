import { Link, withRouter } from "react-router-dom";
import React from "react";
import PropTypes from "prop-types";
import Button from "material-ui/Button";
import Icon from "@material-ui/icons/List";
import Drawer from "material-ui/Drawer";
import Buttons from "./Buttons";
import logo from "../../assets/images/logo.png";

class TemporaryDrawer extends React.Component {
  state = {
    left: false,
  };

  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open,
    });
  };

  render() {
    const { location: { pathname } } = this.props;
    return (
      <div>
        <Button style={{ minWidth: "50px", float: "left" }} onClick={this.toggleDrawer("left", true)}><Icon /></Button>
        <Drawer open={this.state.left} onClose={this.toggleDrawer("left", false)}>
          <div
            tabIndex={0}
            role="button"
            onClick={this.toggleDrawer("left", false)}
            onKeyDown={this.toggleDrawer("left", false)}
          >
            <div style={{ margin: "1em 1em 1em" }}>
              <img src={ logo } height="30" width="140" alt="logo" />
            </div>
            <div>
              <Link to="/" style={ { textDecoration: "none" } }>
                <Buttons
                  style={{ fontWeight: "bold", fontFamily: "-apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Ubuntu,Arial,sans-serif", position: "relative" }}
                  label="Frequent Flyer"
                  color={ pathname === "/" ? "secondary" : "primary"}
                  variant="flat"
                />
              </Link>
            </div>
            <div>
              <Link to="/pointsreport" style={ { textDecoration: "none" } }>
                <Buttons
                  style={{
                    fontWeight: "bold",
                    fontFamily: "-apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Ubuntu,Arial,sans-serif",
                    position: "relative"
                  }}
                  label="Points Report"
                  color={ pathname === "/pointsreport" ? "secondary" : "primary"}
                  variant="flat"
                />
              </Link>
            </div>
            <div>
              <Link to="/redemption" style={ { textDecoration: "none" } } >
                <Buttons
                  style={{ fontWeight: "bold", fontFamily: "-apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Ubuntu,Arial,sans-serif", position: "relative" }}
                  label="Redemption"
                  color={ pathname === "/redemption" ? "secondary" : "primary"}
                  variant="flat"
                />
              </Link>
            </div>
          </div>
        </Drawer>
      </div>
    );
  }
}

TemporaryDrawer.propTypes = {
  location: PropTypes.objectOf(PropTypes.string).isRequired,
};

export default withRouter(TemporaryDrawer);

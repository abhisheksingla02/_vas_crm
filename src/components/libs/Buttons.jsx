import React, { Component } from "react";
import Button from "material-ui/Button";
import PropTypes from "prop-types";

class Buttons extends Component {
    _handle = (event) => {
      this.props.onClick();
    }
    render() {
      return (
        <span>
          <Button
            {...this.props}
            disabled={this.props.disabled}
            variant={ this.props.variant }
            color={ this.props.color }
            onClick={this._handle}
          >{this.props.icon}{this.props.label}
          </Button>
        </span>
      );
    }
}

Buttons.propTypes = {
  variant: PropTypes.string,
  color: PropTypes.string,
  label: PropTypes.string,
  icon: PropTypes.node,
  onClick: PropTypes.func,
  disabled: PropTypes.bool
};

Buttons.defaultProps = {
  variant: "flat",
  color: "inherit",
  icon: " ",
  label: "",
  onClick: () => {},
  disabled: false
};
export default Buttons;

import React, { Component } from "react";
import PropTypes from "prop-types";
import { MenuItem } from "material-ui/Menu";
import Select from "material-ui/Select";

class Dropdown extends Component {
  state= {
    status: this.props.selectedvalue,
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      status: nextProps.selectedvalue,
    });
  }
  _handleChange = (event) => {
    this.setState({ status: event.target.value });
    this.props.onChange(event.target.value);
  }
  render() {
    return (
      <Select
        {...this.props}
        style={ this.props.style }
        value={ this.state.status }
        onChange={ this._handleChange }
        displayEmpty={ true }
        name="value"
        disabled={this.props.disabled}
      >
        {this.props.items.map(item => (
          <MenuItem
            style={{ fontSize: "15px", fontFamily: "-apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Ubuntu,Arial,sans-serif" }}
            value={item.value}
            key={item.value}
          >
            {item.title}
          </MenuItem>))}
      </Select>
    );
  }
}

Dropdown.propTypes = {
  onChange: PropTypes.func,
  items: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.string,
    title: PropTypes.string
  })),
  selectedvalue: PropTypes.string,
  disabled: PropTypes.bool,
  style: PropTypes.objectOf(PropTypes.any)
};

Dropdown.defaultProps = {
  onChange: () => { },
  items: [],
  selectedvalue: "",
  disabled: false,
  style: {}
};

export default Dropdown;

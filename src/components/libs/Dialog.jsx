import React from "react";
import Card, { CardContent } from "material-ui/Card";
import propTypes from "prop-types";
import Clear from "@material-ui/icons/Clear";
import Button from "material-ui/Button";

const styles = {
  card: {
    maxWidth: "97%",
    height: "5em",
    backgroundColor: "#c3f29e",
    marginLeft: "1em",
    position: "static"
  },

  content: {
    fontFamily: "-apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Ubuntu,Arial,sans-serif",
    textAlign: "left",
    fontWeight: "bold",
    size: "1em"
  }
};
class DialogSimple extends React.Component {
  _handleClose = () => {
  };

  render() {
    return (
      <div>
        <Card style={styles.card}>
          <CardContent
            {...this.props}
            style={ styles.content }
          >
            {this.props.icon}
            {this.props.label}
            <Button style={{ marginLeft: "69em", position: "static" }}>
              <Clear />
            </Button>
          </CardContent>
        </Card>
      </div>
    );
  }
}

DialogSimple.propTypes = {
  label: propTypes.string.isRequired,
  dialogOpen: propTypes.bool.isRequired,
  icon: propTypes.any,
};

DialogSimple.defaultProps = {
  icon: ""
};
export default DialogSimple;

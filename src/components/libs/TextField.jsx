import React, { Component } from "react";
import TextField from "material-ui/TextField";
import PropTypes from "prop-types";

class TextFields extends Component {
  state = {
    status: this.props.selectedvalue
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      status: nextProps.selectedvalue
    });
  }
  _handleChange = (event) => {
    this.setState({ status: event.target.value });
    this.props.onChange(event.target.value);
  }
  render() {
    return (
      <TextField
        {...this.props }
        label={this.props.label}
        value={ this.state.status }
        onChange={ this._handleChange }
        type={this.props.type}
        placeholder={this.props.placeholder}
      />
    );
  }
}
TextFields.propTypes = {
  onChange: PropTypes.func.isRequired,
  selectedvalue: PropTypes.string,
  label: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
};

TextFields.defaultProps = {
  selectedvalue: "",
  label: "",
  type: "",
  placeholder: ""
};
export default TextFields;

import React, { Component } from "react";
import PropTypes from "prop-types";
import { TableCell } from "material-ui/Table";

class Cell extends Component {
  handle = () => {

  }
  render() {
    return (
      <TableCell style={ {
        fontSize: "14px",
        textAlign: "center",
        fontFamily: "-apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Ubuntu,Arial,sans-serif",
        whiteSpace: "nowrap",
        textoverflow: "eclipsis",
        overflow: "hidden"
      } }
      >{this.props.children}
      </TableCell>
    );
  }
}
Cell.propTypes = {
  children: PropTypes.any, //eslint-disable-line
};
export default Cell;

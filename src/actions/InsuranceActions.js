import {
  STATUS_UPDATE,
  FETCH_DATA,
  APPLICATION_HAS_ERRORED,
  FAILURE,
  PAGE_IS_LOADING,
  DIALOG_CLOSE
} from "../actiontypes/InsuranceActionType";
import { buildRequestUrl } from "../utils/Url";
import { updateStatus, getFrequentFlyerData, freqFlyerCRM } from "../constants/allEndpoints";

let url;
const dataNotFound = {
  tripId: "No Data Found",
  createdAt: "No Data Found",
  updatedAt: "No Data Found",
  status: "No Data Found",
};
export const fetchList = (input) => {
  return (dispatch, getState) => {
    const state = getState();
    let currentSearchRequest = { ...state.insurance.searchRequest };
    let filters = { ...currentSearchRequest.filters };
    // let search = { ...currentSearchRequest.search };
    let to = { ...currentSearchRequest.to };
    let from = { ...currentSearchRequest.from };
    let trip = { ...currentSearchRequest.trip };
    let pagination = { ...currentSearchRequest.pagination };
    if (input.filters) {
      filters = { ...filters, ...input.filters };
    }
    /* if (input.search) {
      search = { ...search, ...input.search };
    } */
    if (input.to) {
      to = { ...to, ...input.to };
    }
    if (input.from) {
      from = { ...from, ...input.from };
    }
    if (input.trip) {
      trip = { ...trip, ...input.trip };
    }
    if (input.pagination) {
      pagination = { ...pagination, ...input.pagination };
    }
    currentSearchRequest =
      {
        ...currentSearchRequest, filters, pagination
      };
    dispatch(pageIsLoading("Page is loading"));
    currentSearchRequest = {
      ...currentSearchRequest, filters,
    };
    if (to.toSearch !== "" && from.fromSearch !== "") {
      url = buildRequestUrl({
        endpoint: insuranceCRM + getInsuranceData,
        params: {
          filter: "to",
          filterValue: to.toSearch,
          filter2: "from",
          filterValue2: from.fromSearch,
          pageNo: pagination.PageNo,
          isLastPage: pagination.lastPage

        }
      });
    } else if (trip.tripSearch !== "") {
      url = buildRequestUrl({
        endpoint: insuranceCRM + getInsuranceData,
        params: {
          filter: "tripId",
          filterValue: trip.tripSearch,
          pageNo: pagination.PageNo,
          isLastPage: pagination.lastPage

        }
      });
    } else {
      url = buildRequestUrl({
        endpoint: insuranceCRM + getInsuranceData,
        params: {
          filter: "status",
          filterValue: filters.status,
          pageNo: pagination.PageNo,
          isLastPage: pagination.lastPage

        }
      });
    }
    fetch(url, {
      method: "get",
    }).then(res => res.json())
      .then((response) => {
        currentSearchRequest.pagination.lastPage = response.isLastPage;
        currentSearchRequest.pagination.lastPageNumber = response.totalPages;
        if (response.freqFlyers.length === 0) {
          response.freqFlyers[0] = dataNotFound;
        }
        dispatch({
          type: FETCH_DATA,
          data: { list: response.freqFlyers, currentSearchRequest }
        });
        if (response.status === 500) {
          dispatch(apiFailure("Bad response from server"));
        }
      }).catch((e) => {
        //  dispatch(applicationHasErrored(e.message));
        console.log(e.message);  //eslint-disable-line
      });
  };
};
export const DialogClose = (input) => {
  return (dispatch) => {
    const { dialogStatus } = input;
    dispatch({
      type: DIALOG_CLOSE,
      data: dialogStatus
    });
  };
};
export const statusUpdate = (input) => {
  return (dispatch) => {
    const {
      rowIndex,
      freqFlyerStatus,
      dialogStatus,
      verificationId
    } = input;
    url = buildRequestUrl({
      endpoint: freqFlyerCRM + updateStatus,
      params: {
        freqFlyerStatus,
        verificationId
      }
    });

    fetch(url, {
      method: "PUT",
    }).then(res => res.json())
      .then((response) => {
        dispatch({
          type: STATUS_UPDATE,
          data: {
            responseStatus: 404,
            rowIndex,
            freqFlyerStatus,
            dialogStatus
          }
        });
        window.alert("updated successfully");
      }).catch((e) => {
        //  dispatch(applicationHasErrored(e.message));
        console.log(e.message);  //eslint-disable-line
      });
  };
};

export function applicationHasErrored(message) {
  return {
    type: APPLICATION_HAS_ERRORED,
    hasErrored: true,
    errorMessage: message
  };
}
export function pageIsLoading(message) {
  return {
    type: PAGE_IS_LOADING,
    isLoading: true,
    loadMessage: message
  };
}
export function apiFailure(message) {
  return {
    type: FAILURE,
    hasFailure: true,
    apiMessage: message
  };
}
export function itemsFetchDataSuccess(items) {
  return {
    type: "ITEMS_FETCH_DATA_SUCCESS",
    items
  };
}

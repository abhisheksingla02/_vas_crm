import React, { Component } from "react";
import Download from "@material-ui/icons/FileDownload";
import Buttons from "../components/libs/Buttons";
import FootSection from "../components/libs/FootSection";
import { styles } from "../css/PointReportCss";
import { downloadConnectorReport, insuranceService } from "../constants/allEndpoints";

class InsuranceReport extends Component {
 _handleDownload = () => {
 }
 render() {
   return (
     <div >
       <div style={ styles.div }>
         <h3>Download Insurance report</h3>
         <a
           href={insuranceService + downloadConnectorReport }
           style={{ textDecoration: "none" }}
         >
           <Buttons
             style={{ fontFamily: "-apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Ubuntu,Arial,sans-serif" }}
             variant="raised"
             color="primary"
             label="Download"
             icon={<Download />}
           />
         </a>
       </div>
       <FootSection />
     </div>
   );
 }
}
export default InsuranceReport;

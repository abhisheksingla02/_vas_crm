import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
// import Snackbar from "material-ui/Snackbar";
import Download from "@material-ui/icons/FileDownload";
import Grid from "material-ui/Grid";
import NavRight from "@material-ui/icons/ChevronRight";
import NavLeft from "@material-ui/icons/ChevronLeft";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import Search from "@material-ui/icons/Search";
import { fetchList, statusUpdate /* DialogClose */ } from "../actions/InsuranceActions";
import FFTable from "../components/libs/Table";
import styles from "../css/InsuranceCss";
import TextFields from "../components/libs/TextField";
import Buttons from "../components/libs/Buttons";
import FootSection from "../components/libs/FootSection";
import DropDown from "../components/libs/Dropdown";
import { headers, status } from "../constants/Insurance";
import FrequentFlyerRow from "../components/frequentflyer/FrequentFlyerRow";
import { download, insuranceService } from "../constants/allEndpoints";


const paginationCss = {
  margin: { marginLeft: "52em" },
  mobileMargin: { marginLeft: "1em" },
  paginationBorder: {
    position: "relative",
    backgroundColor: "rgb(162, 162, 162)",
    borderRadius: 4,
    marginTop: "20px",
    marginLeft: "20px",
    marginRight: "20px",
    boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"
  },
  paginationIconColor: { color: "white" }
};

// const dialogOpen = true;

class Insurance extends Component {
  state = {
    /* EmailSearch: {
      search: this.props.searchRequest.search
    }, */
    TripSearch: {
      trip: this.props.searchRequest.trip
    },
    To: {
      to: this.props.searchRequest.to
    },
    From: {
      from: this.props.searchRequest.from
    }
  }

  componentDidMount() {
    this.props.fetchList(this.props.searchRequest);
  }

  _handleChangeFilters = (filterValue) => {
    const input = {
      filters: {
        status: filterValue
      },
      pagination: {
        lastPage: false
      }
    };
    this.props.fetchList(input);
  }

  /*  _handleChangeSearch = (searchValue) => {
    const input = {
      search: {
        emailSearch: searchValue
      }
    };
    this.setState({ EmailSearch: input });
  } */
   _handleChangeToSearch = (searchValue) => {
     const input = {
       to: {
         toSearch: searchValue
       }
     };
     this.setState({ To: input });
   }
   _handleChangeFromSearch = (searchValue) => {
     const input = {
       from: {
         fromSearch: searchValue
       }
     };
     this.setState({ From: input });
   }
   _handleChangeTripSearch = (searchValue) => {
     const input = {
       trip: {
         tripSearch: searchValue
       }
     };
     this.setState({ TripSearch: input });
   }

  // handles Pagination logic
  _handleChangePage = nav => (event) => {
    const { searchRequest: { pagination } } = this.props;
    let page = pagination.PageNo;
    let lastPageValue = pagination.lastPage;
    if (nav === "start") {
      page = 1;
      lastPageValue = false;
    } else if (nav === "next" && lastPageValue !== true) {
      page += 1;
    } else if (nav === "end") {
      lastPageValue = true;
      page = pagination.lastPageNumber;
    } else if (nav === "prev") {
      page -= 1;
      lastPageValue = false;
    }
    if (page < 1) {
      page = 1;
    }
    const input = {
      pagination: {
        PageNo: page,
        lastPage: lastPageValue
      }
    };
    this.props.fetchList(input);
  }

  _handleChangeSorters = (sortColumn, sortOrder) => {
    const input = {
      sorters: {
        [sortColumn]: sortOrder
      }
    };
    this.props.fetchList(input);
  }

  _EmailSearch = () => {
    /* const rules = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
    const isValid = rules.test(this.state.EmailSearch.search.emailSearch);
    if (isValid) {
      this.props.fetchList(this.state.EmailSearch);
    } else {
      window.alert("Enter a valid email address");
    } */
  }
  _TripSearch = () => {
    const rules = /[0-9]$/;
    const isValid = rules.test(this.state.TripSearch.trip.tripSearch);
    if (isValid) {
      this.props.fetchList(this.state.TripSearch);
    } else {
      window.alert("Enter a valid tripID");
    }
  }
  _SectorSearch = () => {
    const rules = /[a-zA-Z]$/;
    const isValidTo = rules.test(this.state.To.to.toSearch);
    const isValidFrom = rules.test(this.state.From.from.fromSearch);
    if (!isValidTo && !isValidFrom) {
      window.alert("Enter a valid to and from");
    } else if (!isValidTo) {
      window.alert("Enter a valid to");
    } else if (!isValidFrom) {
      window.alert("Enter a valid from");
    } else {
      this.props.fetchList(this.state.From);
      this.props.fetchList(this.state.To);
    }
  }
  render() {
    const { list, searchRequest } = this.props;
    // Used for different screen sizes
    const placeholder = window.innerWidth >= "700" ? "" : "Email";
    const searchButtonLabel = window.innerWidth >= "700" ? "Search" : "";
    const buttonSize = window.innerWidth <= "410" ? "61px" : "88px";
    // ///////////////////////
    return (
      <div style={styles.wrapper}>
        <span style={styles.heading}>Value Added Services CRM</span>
        <Grid
          container={true}
          style={{
            display: "flex",
            justifyContent: "flex-center",
            paddingLeft: "4.455em",
            marginBottom: "1.455em",
            marginTop: "0.4em"
          }}
        >
          <Grid item={true} style={{ marginRight: "0.6em" }}>
            {
              window.innerWidth >= "700" ?
                <span style={styles.textProperties}>From{"\u00A0"}</span>
                : ""
            }
            <TextFields
              style={ styles.searchBox }
              onChange={this._handleChangeFromSearch}
              selectedvalue={this.state.From.from.fromSearch}
              placeholder={placeholder}
            />
          </Grid>
          <Grid item={true} style={{ marginRight: "0.6em" }}>
            {
              window.innerWidth >= "700" ?
                <span style={styles.textProperties}>To{"\u00A0"}</span>
                : ""
            }
            <TextFields
              style={ styles.searchBox }
              onChange={this._handleChangeToSearch}
              selectedvalue={this.state.To.to.toSearch}
              placeholder={placeholder}
            />
            <Buttons
              style={{
                fontWeight: "bold",
                boxShadow: "0 5px 9px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)",
                minWidth: buttonSize
              }}
              variant="raised"
              color="primary"
              label={searchButtonLabel}
              icon={ <Search /> }
              onClick={ this._SectorSearch }
            />
          </Grid>
          <Grid item={true} style={{ marginRight: "0.6em" }}>
            {
              window.innerWidth >= "700" ?
                <span style={styles.textProperties}>TripId{"\u00A0"}</span>
                : ""
            }
            <TextFields
              style={styles.searchBox}
              onChange={this._handleChangeTripSearch}
              selectedvalue={this.state.TripSearch.trip.tripSearch}
              placeholder={placeholder}
            />
            <Buttons
              style={{
                fontWeight: "bold",
                boxShadow: "0 5px 9px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)",
                minWidth: buttonSize
              }}
              variant="raised"
              color="primary"
              label={searchButtonLabel}
              icon={<Search />}
              onClick={this._TripSearch}
            />
          </Grid>
          <Grid item={true} style={{ marginBottom: "0.6em", float: "right" }}>
            <span style={styles.textProperties}>Status  {"\u00A0"} </span><DropDown
              height="20px"
              style={styles.textPropertiesDropDown}
              items={status}
              selectedvalue={searchRequest.filters.status}
              onChange={this._handleChangeFilters}
            />
          </Grid>
        </Grid>
        <div style={{ textAlign: "center" }}>
          <h3>Download Insurance Report For Internal Reference</h3>
          <a
             href={insuranceService + download }
            style={{
              textDecoration: "none",
              display: "flex",
              justifyContent: "center",
              paddingLeft: "4.455em",
              marginBottom: "1.455em",
              marginTop: "0.4em"
            }}
          >
            <Buttons
              style={{ fontFamily: "-apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Ubuntu,Arial,sans-serif" }}
              variant="raised"
              color="primary"
              label="Download"
              icon={<Download />}
            />
          </a>
        </div>

        <div style={{
          ...styles.table,
        }}
        >
          <FFTable
            headers={headers}
            searchRequest={searchRequest}
            handleChangeFilters={this._handleChangeFilters}
          >
            {list.map((flyer, rowIndex) => {
              return (
                <FrequentFlyerRow
                  key={flyer.tripId}
                  flyer={flyer}
                  id={this.props.id.tripId}
                  rowIndex={rowIndex}
                  searchRequest={searchRequest}
                  onStatusUpdate={this.props.statusUpdate}
                  onDialogClose={this.props.DialogClose}
                />
              );
            })}
          </FFTable>
        </div>

        { /* pagination structure */}

        <div style={paginationCss.paginationBorder}>
          <span
            style={{
              color: "white",
              marginLeft: "1em",
              fontFamily: "-apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Ubuntu,Arial,sans-serif"
            }}
          >
            {window.innerWidth >= "600" ? `${searchRequest.pagination.PageNo}-${searchRequest.pagination.lastPageNumber}` : ""}
          </span>
          <span style={window.innerWidth > "1265" ? paginationCss.margin : paginationCss.mobileMargin}>
            <Buttons style={{ minWidth: buttonSize }} onClick={this._handleChangePage("start")} icon={<FirstPage style={paginationCss.paginationIconColor} />} />
            <Buttons style={{ minWidth: buttonSize }} onClick={this._handleChangePage("prev")} icon={<NavLeft style={paginationCss.paginationIconColor} />} />
            <Buttons style={{ minWidth: buttonSize }} onClick={this._handleChangePage("next")} icon={<NavRight style={paginationCss.paginationIconColor} />} />
            <Buttons style={{ minWidth: buttonSize }} onClick={this._handleChangePage("end")} icon={<LastPage style={paginationCss.paginationIconColor} />} />
          </span>
        </div>
        {/* ////////////////////// */}

        {/* <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left",
          }}
          open={this.props.isLoading}
          autoHideDuration={6000}
          onClose={this.handleClose}
          SnackbarContentProps={{
            "aria-describedby": "message-id",
          }}
          message={<span id="message-id">{this.props.loadMessage}</span>}
          action={[]}
        />
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left",
          }}
          open={this.props.hasFailure}
          autoHideDuration={6000}
          onClose={this.handleClose}
          SnackbarContentProps={{
            "aria-describedby": "message-id",
          }}
          message={<span id="message-id">{this.props.apiMessage}</span>}
          action={[]}
        />
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left",
          }}
          open={this.props.hasErrored}
          autoHideDuration={6000}
          onClose={this.handleClose}
          SnackbarContentProps={{
            "aria-describedby": "message-id",
          }}
          message={<span id="message-id">{this.props.errorMessage}</span>}
          action={[]}
        /> */}
        <FootSection />
      </div>
    );
  }
}

Insurance.propTypes = {
  fetchList: PropTypes.func.isRequired,
  statusUpdate: PropTypes.func.isRequired,
  DialogClose: PropTypes.func.isRequired,
  searchRequest: PropTypes.objectOf(PropTypes.any).isRequired,
  list: PropTypes.arrayOf(PropTypes.object),
  id: PropTypes.number,
  /* hasErrored: PropTypes.bool,
  hasFailure: PropTypes.bool,
  isLoading: PropTypes.bool,
  errorMessage: PropTypes.string,
  apiMessage: PropTypes.string,
  loadMessage: PropTypes.string, */
};
Insurance.defaultProps = {
  list: [],
  id: 0,
  /* hasErrored: false,
  hasFailure: false,
  isLoading: false,
  errorMessage: "",
  apiMessage: "",
  loadMessage: "", */
};

const mapStateToProps = (state) => {
  return {
    list: state.insurance.list,
    id: state.insurance.id,
    searchRequest: state.insurance.searchRequest,
    /* hasErrored: state.brb.hasErrored,
    errorMessage: state.brb.errorMessage,
    hasFailure: state.brb.hasFailure,
    apiMessage: state.brb.apiMessage,
    loadMessage: state.brb.loadMessage,
    isLoading: state.brb.isLoading,
    dialogOpen: state.brb.defaultOpen */

  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchList: (input) => {
      dispatch(fetchList(input));
    },
    statusUpdate: (input) => {
      dispatch(statusUpdate(input));
    },
    /* DialogClose: (input) => {
      dispatch(DialogClose(input));
    }, */
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Insurance);

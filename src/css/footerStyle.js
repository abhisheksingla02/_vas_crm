// ##############################
// // // Footer styles
// #############################

export const footerStyle = {

  left: {
    float: "left!important",
    display: "block"
  },
  right: {
    padding: "15px 0",
    margin: "0",
    fontSize: "14px",
    float: "right!important"
  },
  footer: {
    bottom: "0",
    marginBottom: "0",
    padding: "15px 0",
    marginTop: "13%",
    width: "100%",
    fontFamily: "-apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Ubuntu,Arial,sans-serif",
  },
  a: {
    textDecoration: "none",
    backgroundColor: "transparent"
  },

  inlineBlock: {
    display: "inline-block",
    paddingTop: "0px",
    width: "auto"
  }
};
export default footerStyle;

export const styles = {
  root: {
    marginTop: 20
  },
  card: {
    marginTop: "2%",
    marginBottom: "12%",
    fontWeight: "400",
    fontSize: "25px",
    paddingLeft: "10px",
    paddingRight: "10px",
  },
  searchDiv1: {
    marginLeft: "190px"
  },
  searchDiv2: {
    marginLeft: "900px"
  },
  table: {
    marginTop: "40px",
    marginBottom: "40px",
    paddingLeft: "10px",
    paddingRight: "10px",
  },
  searchBox: {
    marginRight: 10,
    fontFamily: "-apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Ubuntu,Arial,sans-serif"
  },
  tableHistory: {
    marginTop: "40px",
    marginBottom: "40px",
    paddingLeft: "10px",
    paddingRight: "10px",
  },
  heading: {
    fontSize: 25,
    paddingLeft: 20,
    paddingRight: 20,
    fontFamily: "-apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Ubuntu,Arial,sans-serif",
  },
  textPropertiesDropDown: {
    fontFamily: "-apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Ubuntu,Arial,sans-serif",
    fontSize: "16px"
  },
  textProperties: {
    fontFamily: "-apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Ubuntu,Arial,sans-serif",
    fontSize: "18px",
    marginRight: "10px"
  }
};

export const styles2 = { };

export default {
  wrapper: {
    marginTop: 10,
    marginBottom: 10,
    position: "relative",
  },
  clearfix: {
    clear: "both",
  },
  heading: {
    fontWeight: "400",
    fontFamily: "-apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Ubuntu,Arial,sans-serif",
    fontSize: "25px",
    textAlign: "left",
    marginLeft: "1em",
    marginTop: "0.5em"
  },
  dropdown: {
    marginBottom: 20,
    fontSize: "100",
  },
  container: {
    float: "right",
  },
  searchBox: {
    marginRight: 10,
    fontFamily: "-apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Ubuntu,Arial,sans-serif"
  },
  table: {
    // overflowX: "auto",
    paddingRight: "20px",
    paddingLeft: "20px"
  },
  textfield: {
    float: "left",
    marginBottom: 10,
  },
  bod: {
    TextAlign: "middle"
  },
  textPropertiesDropDown: {
    fontFamily: "-apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Ubuntu,Arial,sans-serif",
    fontSize: "16px",
    marginRight: "1.5em"
  },
  textProperties: {
    fontFamily: "-apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Ubuntu,Arial,sans-serif",
    fontSize: "18px",
    paddingRight: "2px",
  },
  row: {
    backgroundColor: "#f2f2f2", fontSize: "16px", textAlign: "center", fontFamily: "-apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Ubuntu,Arial,sans-serif"
  }
};

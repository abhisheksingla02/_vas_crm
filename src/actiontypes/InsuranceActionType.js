export const STATUS_UPDATE = "insurance/STATUS_UPDATE";
export const EMAIL_UPDATE = "insurance/Email_UPDATE";
export const FETCH_DATA = "insurance/FETCH_DATA";
export const APPLICATION_HAS_ERRORED = "insurance/APPLICATION_HAS_ERRORED";
export const FAILURE = "insurance/FAILURE";
export const PAGE_IS_LOADING = "insurance/PAGE_IS_LOADING";
export const INSURANCE_PARAMS = "insurance/INSURANCE_PARAMS";
export const DIALOG_CLOSE = "insurance/DIALOG_CLOSE";
